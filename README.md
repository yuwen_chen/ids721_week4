# ids721_week4

This repo is for the week 4 mini project for course IDS721.

## Step 1: Rust Actix web app
***
cargo init ids721_week4
***
Reference page: https://actix.rs/docs/getting-started/

## Step 2: Build docker image
create Dockerfile
***
docker build -t ids721_week4
***

## Step 3: Build container
***
docker run --name ids721_container -p 8080:8080 ids721_week4
docker start ids721_container
docker attach ids721_container
***

![image](webpage.png)
![image](image.png)
![image](container.png)


