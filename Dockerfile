FROM rust:1.75 AS builder
WORKDIR /Users/yuwenchen/Documents/GitHub/ids721_week4
COPY . .
RUN cargo build --release
## set the same port number
EXPOSE 8080
## start the rust
CMD cargo run